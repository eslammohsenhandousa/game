<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            'Administrator' => [
                'settings.index',
                'settings.edit',
                'users.index',
                'users.create',
                'users.edit',
                'users.destroy',
                'users.activate',
                'users.deactivate',
                'contact-us.index',
                'contact-us.show',
                'contact-us.edit',
                'contact-us.destroy',
                'contact-us.types.index',
                'contact-us.types.create',
                'contact-us.types.edit',
                'contact-us.types.destroy',
                'contact-us.types.activate',
                'roles.index',
                'roles.edit'
            ],
        ];

        foreach ($roles as $role => $permissions) {
            $getRole = Role::firstOrCreate([
                'name' => $role,
                'guard_name' => 'web'
            ]);
            foreach ($permissions as $permission) {
                Permission::firstOrCreate(['name' => $permission]);
                $getRole->givePermissionTo($permission);
            }
        }
    }
}
