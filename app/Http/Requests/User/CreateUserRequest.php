<?php

namespace App\Http\Requests\User;

use App\Rules\ValidateUserName;
use App\Rules\ValidateUserPassword;
use App\Rules\ValidateUserPhone;
use Illuminate\Foundation\Http\FormRequest;

class CreateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [ 'required', 'string', 'max:50', 'unique:users,name', new ValidateUserName()],
            'email'  => 'required|email|max:250|unique:users,email|regex:/@(.+)\./',
            'phone'  => ['nullable', 'numeric', 'digits_between:1,12', 'unique:users,phone'],
            'password' => ['required', 'max:20'],
            // 'role' => 'required|exists:roles,id',
            'avatar' => 'nullable|image|mimes:png,jpg,jfif|max:10240',
        ];
    }
}
