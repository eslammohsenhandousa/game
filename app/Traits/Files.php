<?php

namespace App\Traits;

use Illuminate\Support\Facades\Storage;

trait Files {

    public function uploadFiles($file, $path, $old = null)
    {
        $img = Storage::put($path, $file);   
        // if($old) Storage::delete($old->avatar);
        return $img;
    }

}