<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class ValidateUserPhone implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $start_number = substr($value, 0, 2);
        return str_contains($start_number, '5');
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('Phone number should start with 5xxxxxxx');
    }
}
