<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class ValidateUserName implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if(count(explode(" ",$value)) <= 1) {
            return false;
        }
        return preg_match("/^[\pL\s\d\-]+$/u", $value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('Username should containts at least two words and characters and numbers only');
    }
}
