<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class DateValidation implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return preg_match('/^\d{4}[\/\-]((0?[1-6])[\/\-](0?[1-9]|[12][0-9]|3[01])|(0?[7-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|31))$/', $value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('The date format is wrong');
    }
}
