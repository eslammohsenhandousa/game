<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class ValidateUserPassword implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return preg_match("/^\S*(?=\S{8,})(?=\S*[a-zA-Zي-أ])(?=\S*[0-9])\S*$/", $value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('Password should be at least 8 characters and numbers');
    }
}
