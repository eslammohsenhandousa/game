<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        @include('head')
        <title inertia>Game</title>

        @routes
        @viteReactRefresh
        @vite('resources/js/app.jsx')
        @inertiaHead
    </head>
	<body class="main-body leftmenu">
        {{--  
        <div id="global-loader">
            <img src="assets/img/loader.svg" className="loader-img" alt="Loader" />
        </div> --}}

        @inertia
        {{-- <div class="page">
            <!-- Main Header-->
            @include('header')
            <!-- End Main Header-->
            
            @include('menu')
    
            <!-- Main Content-->
            <div class="main-content side-content pt-0">
                <div class="main-container container-fluid">
                    <div class="inner-body">
                        <!-- Page Header -->
                        <div class="page-header">
                            <div class="d-flex">
                                <div class="justify-content-center">
                                    @yield('buttons')
                                </div>
                            </div>
                        </div>
                        <!-- End Page Header -->
                        <!-- Row -->
                        @yield('PageContent')
                        <!-- End Row -->
                    </div>
                </div>
            </div>
            <!-- End Main Content-->
    
            <!-- Main Footer-->
            <div class="main-footer text-center">
                <div class="container">
                    <div class="row row-sm">
                        <div class="col-md-12">
                            <span>Copyright © 2022 <a href="javascript:void(0)">Spruha</a>. Designed by <a
                                    href="https://www.spruko.com/">Spruko</a> All rights reserved.</span>
                        </div>
                    </div>
                </div>
            </div>
            <!--End Footer-->
        </div> --}}
        <!-- End Page -->
        @include('scripts')
    </body>
</html>
