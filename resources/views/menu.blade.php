<div class="sticky">
    <div class="main-menu main-sidebar main-sidebar-sticky side-menu">
        <div class="main-sidebar-header main-container-1 active">
            <div class="sidemenu-logo">
                <a class="main-logo" href="{{route('dashboard')}}">
                    <img src="{{asset('assets/img/logo.png')}}" height="45px;" class="header-brand-img desktop-logo" alt="logo">
                    <img src="{{asset('assets/img/logo.png')}}" height="45px;" class="header-brand-img icon-logo" alt="logo">
                    <img src="{{asset('assets/img/logo.png')}}" class="header-brand-img desktop-logo theme-logo" alt="logo">
                    <img src="{{asset('assets/img/logo.png')}}" class="header-brand-img icon-logo theme-logo" alt="logo">
                </a>
            </div>
            <div class="main-sidebar-body main-body-1">
                <div class="slide-left disabled" id="slide-left"><i class="fe fe-chevron-left"></i></div>
                <ul class="menu-nav nav">
                    <li class="nav-header"><span class="nav-label">@lang('Dashboard')</span></li>
                    <li class="nav-item @if( Str::contains(Route::currentRouteName(), 'home') ) active @endif">
                        <a class="nav-link" href="{{ route('dashboard') }}">
                            <span class="shape1"></span>
                            <span class="shape2"></span>
                            <i class="ti-home sidemenu-icon menu-icon "></i>
                            <span class="sidemenu-label">@lang('Home')</span>
                        </a>
                    </li>
                    <li class="nav-item @if( Str::contains(Route::currentRouteName(), 'contacts') ) active @endif">
                        <a class="nav-link" href="{{ route('contacts.index') }}">
                            <span class="shape1"></span>
                            <span class="shape2"></span>
                            <i class="ti-server sidemenu-icon menu-icon "></i>
                            <span class="sidemenu-label">@lang('Contact Us')</span>
                        </a>
                    </li>
                    <li class="nav-item @if( Str::contains(Route::currentRouteName(), 'settings') ) active @endif">
                        <a class="nav-link" href="{{ route('settings.index') }}">
                            <span class="shape1"></span>
                            <span class="shape2"></span>
                            <i class="ti-settings sidemenu-icon menu-icon "></i>
                            <span class="sidemenu-label">@lang('Settings')</span>
                        </a>
                    </li>
                </ul>
                <div class="slide-right" id="slide-right"><i class="fe fe-chevron-right"></i></div>
            </div>
        </div>
    </div>
</div>