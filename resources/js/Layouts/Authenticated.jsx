import React, { useState } from 'react';
import ApplicationLogo from '@/Components/ApplicationLogo';
import ResponsiveNavLink from '@/Components/ResponsiveNavLink';
import { Head, Link } from '@inertiajs/inertia-react';
import Menu from '@/Includes/Menu';
import Header from '@/Includes/Header';
import Breadcrumb from '@/Includes/Breadcrumb';
import Footer from '@/Includes/Footer';

export default function Authenticated({ auth, buttons, children }) {
    const [showingNavigationDropdown, setShowingNavigationDropdown] = useState(false);

    return (
        <>
            <div className="page">
                {/* <!-- Main Header--> */}
                <Header auth={auth.user} />
                {/* <!-- End Main Header--> */}
    
                {/* <!-- Sidemenu --> */}
                <Menu />
                {/* <!-- End Sidemenu --> */}

                {/* <!-- Main Content--> */}
                <div className="main-content side-content pt-0">
                    <div className="main-container container-fluid">
                        <div className="inner-body">
                            <div className="page-header">
                                <Breadcrumb />
                                <div className="d-flex">
                                    <div className="justify-content-center">
                                        {buttons}    
                                    </div>
                                </div>
                            </div>
                            {children}
                        </div>
                    </div>
                </div>
                {/* <!-- End Main Content--> */}
                
                {/* <!-- Main Footer--> */}
                <Footer />
                {/* <!--End Footer--> */}
            </div>
        
            {/* <!-- Back-to-top --> */}
            <a href="#top" id="back-to-top"><i className="fe fe-arrow-up"></i></a>
        </>
    );
}
