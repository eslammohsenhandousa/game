import React, { useRef, useState } from "react";
// import '../../../public/assets/plugins/fileuploads/css/fileupload.css';
// import '../../../public/assets/plugins/fileuploads/js/fileupload';

const File = ({name, label, value = '', handleFileChange}) => {
    // $(`.dropify-${ name }`).dropify({
    //     messages: {
    //         'default': "{{ __('Drag and drop a file here or click') }}",
    //         'replace': "{{ __('Drag and drop or click to replace') }}",
    //         'remove': "{{ __('Remove') }}",
    //         'error': "{{ __('Ooops, something wrong appended.') }}"
    //     },
    //     error: {
    //         'fileSize': "{{ __('The file size is too big (2M max).') }}",
    //     },
    //     tpl: {
    //         clearButton: `<button type="button" onclick="deleteImg(${ name })" className="dropify-clear">{{ __('Remove') }}</button>`,
    //     }
    // });
    const fileInput = useRef();
    const [file, setFile] = useState(null);
    function handleFileChange(e) {
        const file = e.target.files[0];
        setFile(file);
    }

    return (
        <div className="form-group">
            <label for={name}>{label}</label>
            <input name={name} id={name} type="file" className={`dropify-`+name}
                data-default-file={file} data-height="200" 
                onChange={handleFileChange} 
                />
            <input type="hidden" id={name+`_deleted`} name={name+`_deleted`} value="0" />
        </div>
    );
}

export default File;