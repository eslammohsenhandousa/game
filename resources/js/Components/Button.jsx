import React from 'react';

export default function Button({ type = 'submit', className = '', processing, children }) {
    return (
        <button
            type={type}
            className={`btn btn-primary `+className}
            disabled={processing}>
            {children}
        </button>
    );
}
