import React, { useEffect, useRef } from 'react';

export default function Input({
    type = 'text',
    name,
    value,
    label = '',
    autoComplete,
    required,
    isFocused,
    handleChange,
    error
}) {
    const input = useRef();
    useEffect(() => {
        if (isFocused) {
            input.current.focus();
        }
    }, []);

    return (
        <div className="form-group">
            <label for={name}>{label} {required && <span class="tx-danger">*</span>}</label>
            <input 
                type={type} 
                className={`form-control ` +( error ?  `is-invalid` : ``) } 
                id={name} 
                name={name} 
                placeholder={label} 
                value={value} 
                ref={input}
                autoComplete={autoComplete}
                required={required}
                onChange={(e) => handleChange(e)}
                />
                {error && 
                    <span className="text-danger">
                        {error}
                    </span>
                }
        </div>
    );
}
