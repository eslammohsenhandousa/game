import React from "react";

const SelectInput = ({ name, label, required, children }) => {
    return (  
        <div className="form-group">
            <label for={name}>{label} {required && <span className="tx-danger">*</span>}</label>
            <select name={name} id={name} className="form-control select2">    
                {children}
            </select>
        </div>
    );
}
export default SelectInput;