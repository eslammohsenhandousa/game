import { Link } from '@inertiajs/inertia-react';
import React from 'react';

const LinkButton = ({ title, url, btnClass = 'primary', iconClass='' }) => {
    return (
        <Link href={url} className={`btn btn-`+btnClass +` btn-icon-text my-2 me-2`}>
            { iconClass && <i className={`fe fe-`+iconClass+` me-2`}></i>}
            { title }
        </Link>
    );
}
export default LinkButton;