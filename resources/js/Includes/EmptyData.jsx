import React from "react";

const EmptyData = ({name}) => {
    return (
        <div className="card-body">
            <div className="text-center">
                <div className="row justify-content-center mt-5">
                    <div className="col-sm-4">
                        <div className="maintenance-img">
                            <img src="assets/img/nodata.svg" className="img-fluid mx-auto d-block" />
                        </div>
                    </div>
                </div>
                <h4 className="mt-5">Let's get started</h4>
                <p className="text-muted">Oops, We don't have {name}.</p>
            </div>
        </div>
    );
}

export default EmptyData;