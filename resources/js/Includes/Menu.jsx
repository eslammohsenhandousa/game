import { Link } from "@inertiajs/inertia-react";
import React, { useState } from "react";
import MenuItem from './MenuItem';

const Menu = () => {
    return (
        <div className="sticky">
            <div className="main-menu main-sidebar main-sidebar-sticky side-menu">
                <div className="main-sidebar-header main-container-1 active">
                    <div className="sidemenu-logo">
                        <Link href={route('dashboard')} className="main-logo">
                            <img src="assets/img/logo-light.png" height="45px;" className="header-brand-img desktop-logo" alt="logo" />
                            <img src="assets/img/icon-light.png" height="45px;" className="header-brand-img icon-logo" alt="logo" />
                            <img src="assets/img/logo.png" className="header-brand-img desktop-logo theme-logo" alt="logo" />
                            <img src="assets/img/logo.png" className="header-brand-img icon-logo theme-logo" alt="logo" />        
                        </Link>
                    </div>
                    <div className="main-sidebar-body main-body-1">
                        <div className="slide-left disabled" id="slide-left"><i className="fe fe-chevron-left"></i></div>
                        <ul className="menu-nav nav">
                            <li className="nav-header"><span className="nav-label">Dashboard</span></li>
                            <MenuItem url={route('dashboard')} name="Home" logo="ti-home" active={route().current('dashboard')} />
                            <MenuItem url={route('users.index')} name="Users" logo="ti-user" active={route().current('users.index')} />
                            <MenuItem url={route('contacts.index')} name="Contacts" logo="ti-server" active={route().current('contacts.index')} />
                            <MenuItem url={route('settings.index')} name="Settings" logo="ti-settings" active={route().current('settings.index')} />
                        </ul>
                        <div className="slide-right" id="slide-right"><i className="fe fe-chevron-right"></i></div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Menu;