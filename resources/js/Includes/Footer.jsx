import { Link } from "@inertiajs/inertia-react";

const Footer = () => {
    return (
        <div className="main-footer text-center">
            <div className="container">
                <div className="row row-sm">
                    <div className="col-md-12">
                        <span>Copyright © {' Year '} <Link href="#">{'App Name'}</Link> Development by Asgatech-php-development. <a href="https://www.asgatech.com/">Asgatech</a> @lang('All rights reserved').</span>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Footer;