import React from "react";
import Dropdown from "@/Components/Dropdown";

const Notifications = () => {
    return (
        <Dropdown>
        <div className="dropdown main-header-notification">
            <Dropdown.Link className="nav-link icon" href="">
                <Dropdown.Trigger>
                    <i className="fe fe-bell header-icons"></i>
                    <span className="badge bg-danger nav-link-badge">4</span>
                </Dropdown.Trigger>
            </Dropdown.Link>
            <div className="dropdown-menu scrollable" id="notifications-div">
                <Dropdown.Content>
                <div className="header-navheading">
                    <p className="main-notification-text">
                        No Notifications found    
                    </p>
                </div>
                <div className="main-notification-list">
                    no fnoa
                </div>
                <div className="dropdown-footer">
                    <Dropdown.Link href="javascript:void(0)">View All Notifications</Dropdown.Link>
                </div>
                </Dropdown.Content>
            </div>
        </div>
        </Dropdown>
    );
}

export default Notifications;