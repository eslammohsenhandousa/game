import { Link } from "@inertiajs/inertia-react";
import React, { useState } from "react";

const Breadcrumb = (props) => {
    
    return (<></>);
    if(props) {
        const [list, setList] = useState([]);
        return (
            <div>
                <h2 className="main-content-title tx-24 mg-b-5">{props.title}</h2>
                <ol className="breadcrumb">
                    <li className="breadcrumb-item">
                        <Link href={route('dashboard')}>
                            Dashboard
                        </Link>
                    </li>
                    {list.map((item) => {
                        <li className="breadcrumb-item">
                            <Link href="{{$item['url']}}">{item.title}</Link>
                        </li>
                    })}
                    <li className="breadcrumb-item active" aria-current="page">{props.title}</li>
                </ol>
            </div>
        );
    } else {
        return (<></>);
    }
}

export default Breadcrumb;