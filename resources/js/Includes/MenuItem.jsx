import React from "react";
import { Link } from "@inertiajs/inertia-react";

const MenuItem = (props) => {
    return (
        <li className={'nav-item ' + (props.active ? 'active' : '')}>
            <Link href={props.url} className="nav-link">
                <span className="shape1"></span>
                <span className="shape2"></span>
                <i className={props.logo +" sidemenu-icon menu-icon"}></i>
                <span className="sidemenu-label">{props.name}</span>
            </Link>
        </li>       
    );
}
export default MenuItem;