import Dropdown from "@/Components/Dropdown";
import { Link } from "@inertiajs/inertia-react";
import React from "react";
import Notifications from "./Notifications";

const Header = ({auth}) => {

    const lang = 'en';
    return (
        <div className="main-header side-header sticky">
            <div className="main-container container-fluid">
                <div className="main-header-left">
                    <Link className="main-header-menu-icon" href="" id="mainSidebarToggle"><span></span></Link>
                    <div className="hor-logo">
                        <Link className="main-logo" href={route('dashboard')}>
                            <img src="assets/img/logo-light.png" height="45px;" className="header-brand-img desktop-logo" alt="logo" />
                            <img src="assets/img/logo-light.png" height="45px;" className="header-brand-img desktop-logo-dark" alt="logo" />
                        </Link>
                    </div>
                </div>
                <div className="main-header-center">
                    <div className="responsive-logo">
                        <Link href={route('dashboard')}>
                            <img src="assets/img/logo.png" height="45px;" className="mobile-logo" alt="logo" />
                        </Link>
                        <Link href={route('dashboard')}>
                            <img src="assets/img/logo.png" height="45px;" className="mobile-logo-dark" alt="logo" />
                        </Link>
                    </div>
                </div>
                <div className="main-header-right">
                    <button className="navbar-toggler navresponsive-toggler" type="button" data-bs-toggle="collapse"
                        data-bs-target="#navbarSupportedContent-4" aria-controls="navbarSupportedContent-4"
                        aria-expanded="false" aria-label="Toggle navigation">
                        <i className="fe fe-more-vertical header-icons navbar-toggler-icon"></i>
                    </button>
                    <div
                        className="navbar navbar-expand-lg  nav nav-item  navbar-nav-right responsive-navbar navbar-dark  ">
                        <div className="collapse navbar-collapse" id="navbarSupportedContent-4">
                            <div className="d-flex order-lg-2 ms-auto">        
                                {/* <!-- Theme-Layout --> */}
                                <div className="dropdown d-flex main-header-theme">
                                    <Link className="nav-link icon layout-setting">
                                        <span className="dark-layout">
                                            <i className="fe fe-sun header-icons"></i>
                                        </span>
                                        <span className="light-layout">
                                            <i className="fe fe-moon header-icons"></i>
                                        </span>
                                    </Link>
                                </div>
                                {/* <!-- Theme-Layout --> */}
                                {/* <!-- country --> */}
                                <div className="dropdown main-header-notification flag-dropdown">
                                    <a className="nav-link icon country-Flag">
                                        <img src="assets/img/flags/en.svg" className="avatar" style={ {'height':'22px'}}  alt="En" />
                                    </a>
                                    <div className="dropdown-menu">
                                            <a href="" className="dropdown-item d-flex ">
                                                <span className="avatar  me-3 align-self-center bg-transparent">
                                                    <img src="assets/img/flags/en.svg" alt={lang} />
                                                </span>
                                                <div className="d-flex">
                                                    <span className="mt-2">{lang}</span>
                                                </div>
                                            </a>
                                    </div>
                                </div>
                                {/* <!-- country --> */}
                                {/* <!-- Full screen --> */}
                                <div className="dropdown ">
                                    <Link className="nav-link icon full-screen-link">
                                        <i className="fe fe-maximize fullscreen-button fullscreen header-icons"></i>
                                        <i className="fe fe-minimize fullscreen-button exit-fullscreen header-icons"></i>
                                    </Link>
                                </div>
                                {/* <!-- Full screen --> */}
                                {/* <!-- Notification --> */}
                                <Notifications />
                                {/* <!-- Notification --> */}
                                {/* <!-- Profile --> */}
                                <div className="dropdown main-profile-menu">
                                    <Link className="d-flex" href="">
                                        <span className="main-img-user">
                                            <img alt="avatar" src="assets/img/user.jpg"/>
                                        </span>
                                    </Link>
                                    <div className="dropdown-menu">
                                        <div className="header-navheading">
                                            <h6 className="main-notification-title">username</h6>
                                            <p className="main-notification-text"> role</p>
                                        </div>
                                        <Link className="dropdown-item border-top" href="">
                                            <i className="fe fe-user"></i> My Profile
                                        </Link>
                                        <Link className="dropdown-item" href="">
                                            <i className="fe fe-power"></i> Sign Out
                                        </Link>
                                    </div>
                                </div>
                                {/* <!-- Profile --> */}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Header;