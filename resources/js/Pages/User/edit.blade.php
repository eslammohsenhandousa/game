@extends('admin.layouts.master')
@section('title',$breadcrumb['title'])
@section('PageContent')
<div class="row row-sm">
    <div @isset($user) class="col-lg-8 col-md-8" @else class="col-lg-12 col-md-12" @endisset>
        <div class="card custom-card">
            @if(isset($user))
                <form id="user_form" action="{{ route('admin.users.update',$user->id) }}" method="post" enctype="multipart/form-data">
            @method('PUT')
            @else
                <form id="user_form" action="{{ route('admin.users.store') }}" method="post" enctype="multipart/form-data">
            @endif
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            @include('admin.component.form_fields.input', [
                                'label' => 'Full Name',
                                'name' => 'name',
                                'type' => 'text',
                                'required' => true,
                                'value' => old('name') ??(isset($user) ? $user->name : null)
                            ])
                            @include('admin.component.form_fields.input', [
                                'label' => 'Email',
                                'name' => 'email',
                                'placeholder' => 'Email',
                                'type' => 'email',
                                'required' => true,
                                'value' => old('email') ??(isset($user) ? $user->email : null)
                            ])
                            @include('admin.component.form_fields.input', [
                                'label' => 'Phone',
                                'name' => 'phone',
                                'placeholder' => 'Enter Phone',
                                'type' => 'text',
                                'value' => old('phone') ??(isset($user) ? $user->phone : null)
                            ])
                            @include('admin.component.form_fields.input', [
                                'label' => isset($user) ? 'New Password' : 'Password',
                                'name' => 'password',
                                'placeholder' => 'Enter password',
                                'type' => 'password',
                                'value' => null
                            ])
                            @include('admin.component.form_fields.image', [
                                'label' => 'Image',
                                'name' => 'avatar',
                                'value' => old('avatar') ?? (isset($user) ? $user->avatar : null)
                            ])
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="role">@lang('Role') <span class="tx-danger">*</span></label>
                                <select name="role" id="role" class="form-control select2 @error('role') is-invalid @enderror">    
                                    <option disabled selected>@lang('Choose type')</option>
                                    @foreach ($roles as $role)
                                        <option value="{{$role->name}}" @if(old('role') == $role->name || isset($user) && ($user->roles->first() && $user->roles->first()->name == $role->name) ) selected @endif> @lang($role->name)</option>
                                    @endforeach
                                </select>
                                @error('role')
                                <span class="invalid-feedback">
                                    {{ $message }}
                                </span>
                                @enderror
                            </div>
                        </div>
                        @canAny('users.activate')
                        @include('admin.component.form_fields.switch', [
                            'label' => 'Active',
                            'name' => 'active',
                            'value' => old('active') ?? (isset($user) ? $user->active : 0)
                        ])
                        @endcanAny
                    </div>
                </div>
                <div class="card-footer mb-1">
                    @csrf
                    <button type="button" onclick="submit_form()" class="btn btn-primary">@lang('Submit')</button>
                    <a href="{{ route('admin.users.index')}}" class="btn btn-danger">@lang('Cancel')</a>
                </div>
            </form>
        </div>
    </div>
    
    @if(isset($user))
    <div class="col-lg-4 col-md-4">
        @include('admin.component.inc.history',[
            'histories' => $user->history()->paginate(10)
        ])
    </div>
    @endif
</div>
@endsection
@push('scripts')
<!-- jQuery Validate js -->
<script src="{{ asset('assets/js/jquery.validate.min.js') }}"></script>
<script>
    function submit_form()
    {
        $('#user_form').validate({
            rules: {
                name : {
                    required : true,
                    maxlength: 50
                },
                email: {
                    required: true,
                    email: true,
                    maxlength: 250
                },
                phone: {
                    maxlength: 12
                },
                password: {
                    minlength: 8,
                    maxlength: 12
                },
            },
            messages: {
                name : {
                    required : "@lang(':attribute is required', ['attribute' => __('Name')])",
                    maxlength: "@lang(':attribute maxlength must not be greater than :number character', ['attribute' => __('Email'), 'number' => 50])"
                },
                email : {
                    required : "@lang(':attribute is required', ['attribute' => __('Email')])",
                    email : "@lang('Email is invalid please enter a valid email')",
                    maxlength: "@lang(':attribute maxlength must not be greater than :number character', ['attribute' => __('Email'), 'number' => 250])"
                },
                phone: {
                    maxlength: "@lang(':attribute maxlength must not be greater than :number character', ['attribute' => __('Phone'), 'number' => 12])"
                },
                password: {
                    minlength: "@lang(':attribute should contains characters and numbers and at laast :number.', ['attribute' => __('Password'), 'number' => 8])",
                    maxlength: "@lang(':attribute maxlength must not be greater than :number characters and numbers', ['attribute' => __('Password'), 'number' => 12])"
                },
            },
            errorClass: "is-invalid text-danger",
            errorPlacement: function (error, element) {
                error.insertAfter(element);
                element.focus();
            },
        });
        if ($('#user_form').valid()) {
            $('#user_form').submit();
        }
    }
</script>
@endpush