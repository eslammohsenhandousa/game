import React from "react";
import Authenticated from '@/Layouts/Authenticated';
import { Head, usePage, Link, useForm } from '@inertiajs/inertia-react';
import LinkButton from "@/Components/LinkButton";
import Input from "@/Components/Input";
import File from "@/Components/File";
import SelectInput from "@/Components/SelectInput";
import Button from "@/Components/Button";

const Create = (props) => {
    
    const {roles , user} = usePage().props;
    
    const { data, setData, errors, post } = useForm({
        name: user ? user.name : '',
        email: user ? user.email : '',
        phone: user ? user.phone : '',
        password: user ? user.password : '',
        avatar: user ? user.avatar : '',
        role: user ? user.role : '',
        _method: user ? 'PUT' : 'POST'
    });
    const onHandleChange = (event) => {
        setData(event.target.name, event.target.type === 'checkbox' ? event.target.checked : event.target.value);
    };
    
    function handleSubmit(e) 
    {
        e.preventDefault();
        if(user) {
            post(route('users.update', user.id));
        } else {
            post(route('users.store'));
        }
    }
    console.log(usePage().props);
    return (
        <Authenticated
            auth={props.auth}
            errors={props.errors}
            header={<h2 className="font-semibold text-xl text-gray-800 leading-tight">Users</h2>}
            buttons={
                <LinkButton title='Create User' url={route('users.create')} iconClass='plus' /> 
            }
            >
            <Head title="Users" />
            <div className="row row-sm">
                <div className="col-lg-12 col-md-12" >
                    <div className="card custom-card">
                        <form onSubmit={handleSubmit} method="post" enctype="multipart/form-data">
                            <div className="card-body">
                                <div className="row">
                                    <div className="col-md-12">
                                        <Input 
                                            type = 'text'
                                            name = 'name'
                                            value = {data.name}
                                            label = 'Full Name'
                                            autoComplete = 'off'
                                            required = {true}
                                            isFocused = {false}
                                            handleChange= {onHandleChange}
                                            error = {props.errors ? props.errors.name : false}
                                        />
                                        <Input
                                            type = 'email'
                                            name = 'email'
                                            value = {data.email}
                                            label = 'Email'
                                            autoComplete = "username"
                                            required = {true}
                                            isFocused = {false}
                                            handleChange= {onHandleChange}
                                            error = {props.errors ? props.errors.email : false}
                                        />
                                        <Input type = 'phone'
                                            name = 'phone'
                                            label = 'Phone'
                                            value = {data.phone}
                                            autoComplete = 'off'
                                            required = {false}
                                            isFocused = {false}
                                            handleChange= {onHandleChange}
                                            error = {props.errors ? props.errors.phone : false}
                                        />
                                        <Input type = 'password'
                                            name = 'password'
                                            label = 'Password'
                                            autoComplete = 'off'
                                            required = {false}
                                            isFocused = {false}
                                            handleChange= {onHandleChange}
                                            error = {props.errors ? props.errors.password : false}
                                        />
                                        <File name='avatar' 
                                            label='Image'
                                            value={data.avatar}                                             
                                            handleChange={onHandleChange}
                                        />
                                        <SelectInput name='role' label='Role'
                                            handleChange = {onHandleChange}
                                            required={true} >
                                            <option disabled selected>Choose type</option>
                                            {roles.map((item) => {
                                                <option value={item.id}>{item.name}</option>
                                            })}
                                        </SelectInput>
                                    </div>
                                    {/* @canAny('users.activate')
                                    @include('admin.component.form_fields.switch', [
                                        'label' => 'Active',
                                        'name' => 'active',
                                        'value' => old('active') ?? (isset($user) ? $user->active : 0)
                                    ])
                                    @endcanAny */}
                                </div>
                            </div>
                            <div className="card-footer mb-1">
                                <Button>
                                    Submit
                                </Button>
                                <Link href={route('users.index')} className="btn btn-danger">
                                    Cancel
                                </Link>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </Authenticated>
    );
}

export default Create;
