@extends('admin.layouts.master')
@section('title', $breadcrumb['title'])
@section('PageContent')

@section('buttons')
    @canAny('users.create')
        @include('admin.component.buttons.btn_href', [
            'title' => __('Create new user'),
            'color_class' => 'primary',
            'url' => route('admin.users.create'),
            'fe_icon' => 'plus'
        ])
    @endcanAny
    @include('admin.component.buttons.filter')
@endsection

<div class="row row-sm">
    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 grid-margin">
        <div class="panel panel-primary tabs-style-3">
            <div class="tab-menu-heading">
                <div class="tabs-menu ">
                    <!-- Tabs -->
                    <ul class="nav panel-tabs">
                        <li class=""><a href="#tab11" class="active" data-bs-toggle="tab"> @lang('Active')</a></li>
                        <li><a href="#tab12" data-bs-toggle="tab" class="">@lang('In Active')</a></li>
                    </ul>
                </div>
            </div>
            <div class="panel-body tabs-menu-body">
                <div class="tab-content">
                    <div class="tab-pane active" id="tab11">
                        <div class="row">
                            @foreach ($roles as $role)
                            <div class="col-lg-3 col-md-6 col-sm-12">
                                <a href="{{ route('admin.users.index', ['role_id' => $role->id, 'active' => 1]) }}">
                                    <div class="card custom-card">
                                        <div class="card-body">
                                            <div class="card-item">
                                                <div class="card-item-icon card-icon">
                                                    <i class="ti-user sidemenu-icon menu-icon"></i>
                                                </div>
                                                <div class="card-item-title mb-2">
                                                    <label class="main-content-label tx-13 font-weight-bold mb-1">@lang($role->name)</label>
                                                </div>
                                                <div class="card-item-body">
                                                    <div class="card-item-stat">
                                                        <h4 class="font-weight-bold text-dark">{{$role->users->where('active', 1)->count()}}</h4>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            @endforeach
                        </div>  
                    </div>
                    <div class="tab-pane" id="tab12">
                        <div class="row">
                            @foreach ($roles as $role)
                            <div class="col-lg-3 col-md-6 col-sm-12">
                                <a href="{{ route('admin.users.index', ['role_id' => $role->id, 'active' => 0]) }}">
                                    <div class="card custom-card">
                                        <div class="card-body">
                                            <div class="card-item">
                                                <div class="card-item-icon card-icon">
                                                    <i class="ti-user sidemenu-icon menu-icon"></i>
                                                </div>
                                                <div class="card-item-title mb-2">
                                                    <label class="main-content-label tx-13 font-weight-bold mb-1">@lang($role->name)</label>
                                                </div>
                                                <div class="card-item-body">
                                                    <div class="card-item-stat">
                                                        <h4 class="font-weight-bold text-dark">{{$role->users->where('active', 0)->count()}}</h4>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            @endforeach
                        </div>  
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 grid-margin">
        <div class="card custom-card">
            @if ($lists->count() > 0)
            <div class="card-body">
                <div class="table-responsive border userlist-table">
                    <table class="table card-table table-striped table-vcenter text-nowrap mb-0">
                        <thead>
                            <tr>
                                <th class="wd-lg-20p"><span></span></th>
                                <th class="wd-lg-8p"><span>@lang('Name')</span></th>
                                <th class="wd-lg-20p"><span>@lang('Email')</span></th>
                                <th class="wd-lg-20p"><span>@lang('Phone')</span></th>
                                <th class="wd-lg-20p"><span>@lang('Role')</span></th>
                                <th class="wd-lg-20p"><span>@lang('Created At')</span></th>
                                @canAny('users.activate')
                                    <th class="wd-lg-20p"><span>@lang('Status')</span></th>
                                @endcanAny
                                <th class="wd-lg-20p">@lang('Action')</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($lists as $list)
                                <tr>
                                    <td>
                                        <img alt="avatar" class="rounded-circle avatar-md me-2" src="{{ display_image_by_model($list,'avatar') }}">
                                    </td>
                                    <td>
                                        @canAny('users.edit')
                                        <a href="{{ route('admin.users.edit',$list->id) }}">
                                            {{ $list->name ?? '' }}
                                        </a>
                                        @else
                                            {{ $list->name ?? '' }}
                                        @endcanAny
                                    </td>
                                    <td>
                                        <a href="mailto:{{ $list->email ?? '' }}">
                                            {{ $list->email ?? '' }}
                                        </a>
                                    </td>
                                    <td>
                                        {{ $list->phone ?? '' }}
                                    </td>
                                    <td>
                                        {{ $list->roles->first() ? __($list->roles->first()->name) : "" }}
                                    </td>
                                    <td>
                                        {{ list_date_time_format( $list->created_at )}}
                                    </td>
                                    @canAny('users.activate')
                                    <td>
                                        @if($list->active)
                                            <span class="label text-success"><span class="dot-label bg-success me-1"></span>@lang('Active')</span>
                                        @else
                                            <span class="label text-muted"><span class="dot-label bg-gray-300 me-1"></span>@lang('In Active')</span>
                                        @endif
                                    </td>
                                    @endcanAny
                                    <td>
                                        @canAny('users.edit')
                                            <a href="{{ route('admin.users.edit',$list->id) }}" class="btn btn-sm btn-info">
                                                <i class="fe fe-edit-2"></i>
                                            </a>
                                        @endcanAny
                                        @canAny('users.destroy')
                                            @include('admin.component.buttons.delete_actions', [
                                                'url' => route('admin.users.destroy',$list->id),
                                            ])
                                        @endcanAny
                                        
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $lists->withQueryString()->withQueryString()->links('admin.layouts.inc.paginator') }}
            </div>
            @else
                @include('admin.component.inc.nodata', [
                    'name' => __('Users')
                ])
            @endif
        </div>
    </div>
</div>

@include('admin.component.modals.filter', [
    'fields' => [
        [
            'name' => 'name',
            'label' => 'Search by name or email or phone number',
            'type' => 'text'
        ],
        [
            'name' => 'role_id',
            'label' => 'Search by Role',
            'type' => 'select',
            'data' => $roles,
            'translate' => true
        ],
        [
            'name' => 'active',
            'label' => 'Status',
            'type' => 'select',
            'data' => [
                [
                    'id' => 1,
                    'name' => 'Active'
                ],
                [
                    'id' => 0,
                    'name' => 'In Active'
                ]
            ],
            'translate' => true
        ]
    ],
    'url' => route('admin.users.index')
])
@include('admin.component.modals.delete')
@endsection