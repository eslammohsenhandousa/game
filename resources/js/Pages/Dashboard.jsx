import React from 'react';
import Authenticated from '@/Layouts/Authenticated';
import { Head } from '@inertiajs/inertia-react';

export default function Dashboard(props) {
    return (
        <> 
        {/* <div id="global-loader">
            <img src="assets/img/loader.svg" className="loader-img" alt="Loader" />
        </div> */}
        <Authenticated
            auth={props.auth}
            errors={props.errors}
            header={<h2 className="font-semibold text-xl text-gray-800 leading-tight">Dashboard</h2>}
        >
            <Head title="Dashboard" />

            Welcome
        </Authenticated>
        </>
    );
}
