import React from "react";
import Authenticated from '@/Layouts/Authenticated';
import { Head, usePage, Link } from '@inertiajs/inertia-react';
import EmptyData from "@/Includes/EmptyData";

const Index = (props) => {
    const { contacts } = usePage().props;
    const { data } = contacts;
    console.log(contacts);
    return (
        <Authenticated
            auth={props.auth}
            errors={props.errors}
            header={<h2 className="font-semibold text-xl text-gray-800 leading-tight">contacts</h2>}
            >
            <Head title="contacts" />
            <div className="row row-sm">
                <div className="col-sm-12 col-md-12 col-lg-12 col-xl-12 grid-margin">
                    <div className="card custom-card">
                        { data.length === 0 && <EmptyData name='users' />}
                        { data.length > 0 && 
                        <div className="card-body">
                            <div className="table-responsive border userlist-table">
                                <table className="table card-table table-striped table-vcenter text-nowrap mb-0">
                                    <thead>
                                        <tr>
                                            <th className="wd-lg-20p"><span></span></th>
                                            <th className="wd-lg-8p"><span>Name</span></th>
                                            <th className="wd-lg-20p"><span>Email</span></th>
                                            <th className="wd-lg-20p"><span>Phone</span></th>
                                            <th className="wd-lg-20p"><span>Created at</span></th>
                                            <th className="wd-lg-20p"><span>Status</span></th>
                                            <th className="wd-lg-20p">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    {data.map((item) => {
                                        return (
                                            <tr key={item.id}>
                                                <td>
                                                    <img alt="avatar" className="rounded-circle avatar-md me-2" src="assets/img/user.jpg" />
                                                </td>
                                                <td>
                                                    <Link href={route('contacts.edit', item.id)}>
                                                        {item.name}
                                                    </Link>
                                                </td>
                                                <td>
                                                    <Link href={route('contacts.edit', item.id)}>
                                                        {item.email}
                                                    </Link>
                                                </td>
                                                <td>
                                                    {item.phone}
                                                </td>
                                                <td>
                                                    {item.created_at}
                                                </td>
                                                <td>
                                                    {/* {item.status && <span class="label text-success"><span class="dot-label bg-success me-1"></span> Active</span>}
                                                    {!item.status && <span class="label text-muted"><span class="dot-label bg-gray-300 me-1"></span> In Active</span>} */}
                                                </td>
                                                <td>
                                                    <Link href={route('contacts.edit', item.id)} class="btn btn-sm btn-info">
                                                        <i class="fe fe-edit-2"></i>
                                                    </Link>
                                                </td>
                                            </tr>
                                        );
                                        })}
                                        {data.length === 0 && <EmptyData name='contacts' />}
                                    </tbody>
                                </table>
                            </div>
                            'links'
                        </div>
                        }
                    </div>
                </div>
            </div>
        </Authenticated>
    );
}

export default Index;
